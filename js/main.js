$(function() {
  Hash.on('^page/([0-9]*)$', {
    yep: function(path, uri_parts) {
      var that = this;
      $('#loader').fadeIn(200, function() {
        $('html, body').animate({
            scrollTop: $("html").offset().top
        }, 400);

        if (uri_parts[1] > 0) {
          $("#parts-list").fadeIn(400);
        } else {
          $("#parts-list").fadeOut(400);
        }

        $('#content').load('content/' + uri_parts[1] + '.html', function(response, status, xhr) {
          switch (status) {
            case 'success':
              $('#loader').fadeOut(1000);
              break;
            case 'error':
              Hash.go('page/0').update();
              that.yep('page/0', ['page/0', '0']);
              break;
          };
        });
      });
    },
    nop: function(path) {
      Hash.go('page/0').update();
      this.yep('page/0', ['page/0', '0']);
    }
  });

  $('#browser-is-not-supported .close-message span').click(function(e) {
    $('#browser-is-not-supported').slideUp(400);
  });
});